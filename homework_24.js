"use strict";

//Упражнение 1

for (let num = 0; num <= 20; num = num + 2) {
    console.log(num)
}

//Упражнение 2
let result = 0;

for (num = 0; num < 3; num++) {
    let value = +prompt(`Введите число:`);
    if (!value) {
        alert(`Ошибка! Вы ввели не число.`);
        break;
    }

    result = result + value;
}

console.log(result);

//Упражнение 3

function getNameOfMonth(month) {

    switch (month) {
        case 0:
            return "Январь";
        case 1:
            return "Февраль";
        case 2:
            return "Март";
        case 3:
            return "Апрель";
        case 4:
            return "Май";
        case 5:
            return "Июнь";
        case 6:
            return "Июль";
        case 7:
            return "Август";
        case 8:
            return "Сентябрь";
        case 9:
            return "Октябрь";
        case 10:
            return "Ноябрь";
        case 11:
            return "Декабрь";
        default:
            break;
    }
}

for (let i = 0; i < 12; i++) {
    if (i === 9) continue;
 
    console.log(getNameOfMonth(i));
}

//Упражнение 4
// Immediately Invoked Function Expression, IIFE — это функция, которая выполняется сразу же после того, как была определена
// ;(function module1() {
//     const a = 1;
//     console.log(a);
//   })()
//   ;(function module2() {
//     const a = 2;
//     alert(a);
//   })()