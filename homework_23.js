"use strict";

//Упражнение 1

let a = '$100';
let b = '300$';
let summ = +(a.replace('$', '')) + +(b.replace('$', ''));

console.log(summ);

//Упражнение 2

let message = ' привет, медвед      ';
message = message.trim().replace('п', 'П');

console.log(message);

//Упражнение 3

let answer = +prompt('Сколько вам лет?');

if (answer <= 3) {
    alert(`Вам ${answer} лет и вы младенец`);
} else if (answer > 3 && answer <= 11) {
    alert(`Вам ${answer} лет и вы ребенок`);
} else if (answer > 11 && answer <= 18) {
    alert(`Вам ${answer} лет и вы подросток`);
} else if (answer > 18 && answer <= 40) {
    alert(`Вам ${answer} лет и вы познаете жизнь`);
} else if (answer > 40 && answer <= 80) {
    alert(`Вам ${answer} лет и вы познали жизнь`);
} else if (answer > 80) {
    alert(`Вам ${answer} лет и вы логожитель`);
}

//Упражнение 4

let message1 = 'Я работаю со строками как профессионал!';
let count = message1.split(' ');

console.log(count);