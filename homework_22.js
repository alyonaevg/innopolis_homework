"use strict";

//Упражнение 1
let a = '100px';
let b = '323px';
let result = parseInt(a) + parseInt(b);

console.log(result);

//Упражнение 2
let maxNum = Math.max(10, -45, 102, 36, 12, -1, 0);
console.log(maxNum);

//Упражнение 3
let d = 0.111;
let ceilNum = Math.ceil(d);
console.log(ceilNum);

let e = 45.333333;
let fixedNum = e.toFixed(1);
console.log(fixedNum);

let f = 3;
let powNum = f ** 5;
console.log(powNum);

let g = 4000000000000000;
g = 4e15;
console.log(g);

let h = '1' != 1;
h = '1' == 1;
console.log(h);

//Упражнение 4
console.log(0.1 + 0.2);
console.log(0.1+0.2===0.3);
//потому что (0.1 + 0.2) будет 0.30000000000000004 из-за особенности хранения в памяти чисел в плавающей точкой
