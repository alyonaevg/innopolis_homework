"use strict";

// Упражнение 1

function exerciseOneSimple() {
    let value = +prompt(`Введите число:`);
    if (!value) {
        alert(`Ошибка! Вы ввели не число.`);
    } else {

        let intervalId = setInterval(() => {
            // Уменьшаем число на один каждую секунду
            value = value - 1;
            // Выводим в консоль новое значение
            console.log(`Осталось ${value}`);
            // Останавливаем интервал, когда дошли до нуля
            if (value === 0) {
                // Передаём в функцию специальный идентификатор
                //ОБЯЗАТЕЛЬНО!
                clearInterval(intervalId);
                alert('Время вышло!')
            }
        }, 1000);

    }
}

function exerciseOnePromise() {
    let counterPromise = new Promise((resolve, reject) => {
        let value = +prompt(`Введите число:`);
        if (!value) {
            reject();
        } else {
            resolve(value);
        }
    })
    
    counterPromise
                // в случае успеха, в параметр enteredNumber попадет 
                // значение переменной value из функции выше
        .then(function resolve(enteredNumber) {
            let intervalId = setInterval(() => {
                // Уменьшаем число на один каждую секунду
                enteredNumber = enteredNumber - 1;
                // Выводим в консоль новое значение
                console.log(`Осталось ${enteredNumber}`);
                // Останавливаем интервал, когда дошли до нуля
                if (enteredNumber === 0) {
                    // Передаём в функцию специальный идентификатор
                    //ОБЯЗАТЕЛЬНО!
                    clearInterval(intervalId);
                    alert('Время вышло!')
                }
            }, 1000);
        })
        .catch(function reject() {
            alert(`Ошибка! Вы ввели не число.`);
        })
}

exerciseOnePromise();

//Упражнение 2

//передаем в функцию адрес бэкэнда
let promise = fetch('https://reqres.in/api/users');

promise
    //если все ок, ответ приходит в каком-то формате, мы его переводим в формат JSON именно
    .then(function (response) {
        return response.json();
    })

    //далее если все ок, то мы можем увидеть содержимое, в нем нас интересует именно массив в данными пользователей
    .then(function (response) {

        //присваиваем переменной изеры этот массив
        let users = response.data;

        //создаем пустое соообщение, в которое далее передадим нужную информацию из массива
        let message = '';

        //собираем по частям сообщение с помощью $(ссылки) на нужные нам ключи
        message = message + (`Получили пользователей: ${users.length}\n`);

        //с помощью метода foreach проходимся по каждому изеру и выцепляем из него с помощью того же $способа нужную инфу
        //НЕ забываем расставить переносы строк через \n (ОБРАТНЫЙ СЛЕШ!)
        users.forEach(function (user) {
            message = message + (`- ${user.first_name} ${user.last_name} (${user.email})\n`);
        });
        console.log(message);
    })

    //выполнится в случае ошибки
    .catch(function () {
        console.log('Error!');
    })