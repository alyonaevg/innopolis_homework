'use strict';
////////////////////////Домашнее задание 28/////////////////////////

//нахожу форму и подписываюсь на событие отправки формы
//сбрасываю поведение формы по умолчанию, чтобы задать в дальнешем поведение при неправильном заполнении
let form = document.querySelector('.review-form');
form.addEventListener('submit', (event) => {
    event.preventDefault();
    //создаю условие при котором будет вызываться лишь одна из функций
    //нам нужно было, чтобы при неправильно заполнениых обоих инпутах,
    //показывалось только первое предупрежение об ошибке
    if (nameInputCorrect()) {
        if (ratingInputCorrect()) {
            clearSavedInputs();
        }
    }
});

//подписываюсь на событие фокуса в инпутах, чтобы убрать вплывающее сообщение(ворнинг),
//когда пользователь хочет исправить/дополнить ввод
let inputs = document.querySelectorAll('input.review-form__validation');
inputs.forEach((input) => input.addEventListener('focus', (event) => {
    //event.target это фактически input
    event.target.style.border = '';
    let warningMessages = document.querySelectorAll('span.review-form__validation');
    warningMessages.forEach((message) => message.style.display = 'none');
}))

function changeBorderColor(input) {
    input.style.border = '1px solid #DA4A0C';
}

function showWarningMessage(warning, message) {
    warning.innerHTML = message;
    warning.style.display = "inline-block";
}

/**
 * Проверяет, правильно ли заполнено поле ФИО, если нет - показывает сообщение.
 * @returns {boolean} возвращает false при ошибке и true если все верно
 */
function nameInputCorrect() {
    let nameInput = document.querySelector('.review-form__input-text');
    let warningName = document.querySelector('.review-form__warning-name');

    if (!nameInput.value) {
        showWarningMessage(warningName, 'Вы забыли указать имя и фамилию');
        changeBorderColor(nameInput);
        return false;
    }

    if (nameInput.value.length < 2) {
        showWarningMessage(warningName, 'Имя не может быть короче 2-х символов');
        changeBorderColor(nameInput);
        return false;
    }
    return true;
}

/**
 * Проверяет, правильно ли заполнено поле Оценка, если нет - показывает сообщение.
 * @returns {boolean} возвращает false при ошибке и true если все верно
 */
function ratingInputCorrect() {
    let ratingInput = document.querySelector('.review-form__input-rating');
    let warningRating = document.querySelector('.review-form__warning-rating');

    if (!ratingInput.value || typeof (+ratingInput.value) !== 'number' || ratingInput.value < 1 || ratingInput.value > 5) {
        showWarningMessage(warningRating, 'Оценка должна быть от 1 до 5');
        changeBorderColor(ratingInput);

        return false;
    }
    return true;
}


////////////////////////////////Домашнее задание 29///////////////////////////

let savedInputs = form.querySelectorAll('.review-form__input');

window.addEventListener('load', handleLoadFormValues);
window.addEventListener('beforeunload', handleSaveFormValues);

function handleLoadFormValues(event) {
    let savedInputValuesJson = window.localStorage.getItem('formValues');
    if (!savedInputValuesJson) return;
    let savedInputValues = JSON.parse(savedInputValuesJson);
    savedInputs.forEach((savedInput) => {
        savedInput.value = savedInputValues[savedInput.name];
    })
}

function handleSaveFormValues(event) {
    event.preventDefault()
    let savedInputValues = {};
    savedInputs.forEach((savedInput) => {
        savedInputValues[savedInput.name] = savedInput.value;
    });
    window.localStorage.setItem('formValues', JSON.stringify(savedInputValues));
}


function clearSavedInputs() {
    savedInputs.forEach((savedInput) => {
        window.localStorage.removeItem('formValues');
        savedInput.value = '';
    })
}

////////////ДЗ 31 - АТТЕСТАЦИЯ

const cartButton = document.querySelector('.add-to-card-btn');
const cartCounter = document.querySelector('.cart');
let cart = [];


cartButton.addEventListener('click', handleClick);
window.addEventListener('load', handleLoadCart);
window.addEventListener('beforeunload', handleSaveCart);

function handleClick(event) {
    //let counterCurrentValue = +cartCounter.getAttribute('data-count');
    const colorInput = document.querySelector('.color-configuration').querySelector('input[type=radio]:checked');
    const memoryInput = document.querySelector('.memory-configuration').querySelector('input[type=radio]:checked');

    if (cart.length === 0) {
        setButtonPressed(event.target);
        let product = getObjectFromInputs(colorInput, memoryInput);
        addToCart(product);
        console.log(cart);
        cartCounter.setAttribute('data-count', cart.length);

        if (cart.length > 0) {
            cartCounter.classList.add('pseudo-shown');
        }
    } else {
        setButtonDefault(event.target);
        //так как по дз может находиться только один товар,
        //удалаю послений добаленный (он же единственный) товар из корзины
        removeFromCart();
        console.log(cart);
        cartCounter.setAttribute('data-count', cart.length);

        if (cart.length === 0) {
            cartCounter.classList.remove('pseudo-shown');
        }
    }
}

function setButtonDefault(button) {
    button.style.backgroundColor = '';
    button.innerHTML = "Добавить в корзину";
}

function setButtonPressed(button) {
    button.innerHTML = "Товар уже в корзине";
    button.style.backgroundColor = '#888888';
}

function handleSaveCart(event) {
    event.preventDefault();
    window.localStorage.setItem('cart', JSON.stringify(cart));
}

function handleLoadCart(event) {
    let cartJson = window.localStorage.getItem('cart');
    if (!cartJson) return;
    cart = JSON.parse(cartJson);
    if (cart.length > 0) {
        setButtonPressed(cartButton);
        cartCounter.setAttribute('data-count', cart.length);
        cartCounter.classList.add('pseudo-shown');
    }
}

function addToCart(item) { 
    if (cart.includes(item) === false) cart.push(item);
}

function removeFromCart(item) {
    cart.pop(item);
}

function getObjectFromInputs(...inputs) {
    let inputValues = {};
    inputs.forEach((input) => {
        inputValues[input.name] = input.value;
    });
    return inputValues;
}
