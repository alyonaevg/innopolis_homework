import {configureStore} from "@reduxjs/toolkit";
import cartReducer from'./reducers/cart-reducer';
import favsReducer from "./reducers/favs-reducer";

export const store = configureStore({
    reducer: {
        cart: cartReducer,
        favs: favsReducer,
    },
}); 