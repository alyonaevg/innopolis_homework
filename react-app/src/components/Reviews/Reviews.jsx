import React from 'react';
import './Reviews.css';
import Review from '../Review/Review';

function Reviews() {
    const reviews = [
        {
            id: 1,
            name: 'Марк Г.',
            imageUrl: '/resources/review-1.jpeg',
            rating: 5,
            usagePeriod: 'менее месяца',
            advantages: `это мой первый айфон после после огромного количества телефонов на андроиде. всё плавно, чётко и красиво. довольно шустрое устройство. камера весьма неплохая, ширик тоже на высоте.`,
            disadvantages: `к самому устройству мало имеет отношение, но перенос данных с андроида - адская вещь) а если нужно переносить фото с компа, то это только через itunes, который урезает качество фотографий исходное`,
        },
        {
            id: 2,
            name: 'Валерий Коваленко',
            imageUrl: '/resources/review-2.jpeg',
            rating: 4,
            usagePeriod: 'менее месяца',
            advantages: `OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго`,
            disadvantages: `Плохая ремонтопригодность`,
        },

    ]
    return (
        <section className="reviews-box specs-reviews-box__reviews-box">
            <div className="reviews-box__header">
                <div className="reviews-box__title">
                    <h3 className="reviews-box__title-title" style={{ display: 'inline-block' }}>Отзывы</h3><span
                        className="reviews-box__title-amount" style={{ color: '#aaaaaa' }}>425</span>
                </div>
            </div>
            <div className="reviews-box__reviews">
                {reviews.map((review) => (
                    <Review key={review.id} review={review} />
                ))}
            </div>

        </section>
    )
}

export default Reviews