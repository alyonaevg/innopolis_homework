import React from 'react'

function PageNotFound() {
  return (
    <h2>Упс... Страница не найдена!</h2>
  )
}

export default PageNotFound
