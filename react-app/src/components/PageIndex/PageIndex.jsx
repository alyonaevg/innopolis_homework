import React from "react";
import './PageIndex.css';

import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import { Link } from "react-router-dom";

function PageIndex() {
    return ( 
    <div className="pageindex">
        <Header />
        <section className="pageindex__main">
            <div className="pi-main__massage">Здесь должно быть содержимое главной страницы.<br />
                Но в рамках курса главная страница  используется лишь
                в демонстрационных целях</div>
            <Link className="pi-main__link" to={'product'} >Перейти на страницу товара</Link>
        </section>
        <Footer />
    </div>
)}

export default PageIndex
