import React, {useState} from 'react';
import './ReviewForm.css';

function ReviewForm() {

    const [name, setName] = useState(localStorage.getItem('name'));
    const [rate, setRate] = useState(localStorage.getItem('rate'));
    const [text, setText] = useState(localStorage.getItem('text'));

    const changeName = (event) => {
        const value = event.target.value;
        localStorage.setItem('name', value);
        setName(value);
    }

    const changeRate = (event) => {
        const value = event.target.value;
        localStorage.setItem('rate', value);
        setRate(value);
    }

    const changeText = (event) => {
        const value = event.target.value;
        localStorage.setItem('text', value);
        setText(value);
    }

    const [errorName, setErrorName] = useState();
    const [errorRate, setErrorRate] = useState();


    function handleSubmit(event) {
        event.preventDefault();

        if (!name) {
            setErrorName('Вы забыли указать имя и фамилию');
            setErrorRate(null);
            return;
        }

        if (name.length < 2) {
            setErrorName('Имя не может быть короче 2-х символов');
            setErrorRate(null);
            return;
        }

        if (!rate || typeof (+rate) !== 'number' || rate < 1 || rate > 5) {
            setErrorRate('Оценка должна быть от 1 до 5');
            setErrorName(null);
            return;
        }

        localStorage.removeItem('name');
        setName('');
        localStorage.removeItem('rate');
        setRate('');
        localStorage.removeItem('text');
        setText('');
    }



    function handleFocus(input) {
        setErrorName(null);
        setErrorRate(null);
    }

    return (
        <div className="review-form-box specs-reviews-box__review-form-box">
            <h3 className="form-box__title">Добавить свой отзыв</h3>
            <form onSubmit={handleSubmit} className="review-form">
                <div className="review-form__flex-1">
                    <div className="review-form__flex-2">
                        {errorName && <span className="review-form__warning-name review-form__validation">{errorName}</span>}
                        <input
                        onInput={(e) => changeName(e)}
                        onFocus={handleFocus}
                        className="review-form__input-text review-form__validation review-form__input"
                        type="text"
                        name="name"
                        placeholder="Имя и фамилия"
                        value={name} 
                        />
                        <input
                        onInput={(e) => changeRate(e)}
                        onFocus={handleFocus}
                        className="review-form__input-rating review-form__validation review-form__input"
                        type="number"
                        // min="1"
                        // max="5"
                        step="1"
                        name="rate"
                        placeholder="Оценка"
                        value={rate}
                        />
                        {errorRate && <span className="review-form__warning-rating review-form__validation">{errorRate}</span>}
                    </div>
                    <textarea
                    onInput={(e) => changeText(e)} 
                    className="review-form__textarea review-form__input" 
                    name="text" placeholder="Текст отзыва" 
                    value={text}></textarea>
                </div>
                <button className="btn review-form-btn" type="submit">Отправить отзыв</button>
            </form>
        </div>)
}

export default ReviewForm