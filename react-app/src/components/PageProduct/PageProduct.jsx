import React from "react";
import Header from '../Header/Header';
import Main from '../Main/Main';
import Footer from '../Footer/Footer';
import "./PageProduct.css";

function PageProduct() {
    return (<div>
        <Header />
        <Main />
        <Footer />
    </div>);
}

export default PageProduct;