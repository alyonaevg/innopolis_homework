import React from "react";
import "./Footer.css";

function Footer () {
    return (<footer className="footer">
        <div className="footer__info">
            <div className="footer__text">
                <div><b>© ООО «<span style={{color:'#f52'}}>Мой</span>Маркет», 2018-2022</b></div>
                <div>Для уточнения информации звоните по номеру
                    <a className="footer__link" href="tel:79000000000">+7 900 000 0000</a>,
                </div>
                <div>а предложения по сотрудничеству отправляйте на почту <a className="footer__link"
                    href="mailto:partner@mymarket.com">partner@mymarket.com</a></div>
            </div>
            <a href="#top" className="footer__link">Наверх</a>
        </div>
    </footer>);
}

export default Footer;