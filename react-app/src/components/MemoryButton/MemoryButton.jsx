import React from 'react';
import './MemoryButton.css';

function MemoryButton(props) {
    const { memoryConfig, active, ...restProps } = props;
    const className = `memory-configuration__btn ${active ? 'memory-configuration__btn_active' : ''}`
    return (
        <label>
            <input type="radio" name="memory-configuration" value={memoryConfig.value} />
            <div {...restProps} className={className}>{memoryConfig.text}</div>
        </label>
    )
}

export default MemoryButton