import React, {useState} from 'react';
import './Colors.css';
import ColorButton from '../ColorButton/ColorButton';

function Colors() {
    const colors = [
        {
            id: 1,
            value: "red",
            imgLink: "resources/color-1.webp",
            alt: "iPhone красного цвета",
        },
        {
            id: 2,
            value: "dark-green",
            imgLink: "resources/color-2.webp",
            alt: "iPhone темно-зеленого цвета",
        },
        {
            id: 3,
            value: "pink",
            imgLink: "resources/color-3.webp",
            alt: "iPhone розового цвета",
        },
        {
            id: 4,
            value: "blue",
            imgLink: "resources/color-4.webp",
            alt: "iPhone синего цвета",
        },
        {
            id: 5,
            value: "white",
            imgLink: "resources/color-5.webp",
            alt: "iPhone белого цвета",
        },
        {
            id: 6,
            value: "black",
            imgLink: "resources/color-6.webp",
            alt: "iPhone черного цвета",
        },
    ]
    const [activeColor, setActiveColor] = useState(5);

    function handleClick(e) {
        setActiveColor(e.key)
    }

    return (
        <div className="color-configuration">
            <h3 className="color-configuration__title">Цвет товара: Синий</h3>
            <div className="color-configuration__radio-buttons">
                {colors.map((color) => (
                    <ColorButton 
                    key={color.id}
                    color={color}
                    active={color.id === activeColor}
                    onClick={handleClick}
                    />
                ))}
            </div>
        </div>
    )
}

export default Colors