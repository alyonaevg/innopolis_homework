import React from 'react';
import './Review.css';

function Review(props) {
    const { review } = props;
    return (
        <>
        <div className="reviews-box__review">
            <img className="reviews-box__photo" src={review.imageUrl} />
            <div className="reviews-box__review-body">
                <div className="reviews-box__review-title">
                    <h3 className="reviews-box__review-title-name">{review.name}</h3>
                    <div className="reviews-box__rating">
                        { [1,2,3,4,5].map( (i) => ( review.rating >= i ? <img key={i} src="../resources/star.svg" height="30px" /> : <img key={i} src="../resources/star-empty.svg" height="30px" /> ))}
                    </div>
                </div>
                <div className="reviews-box__text">
                    <p className="reviews-box__paragraph"><b>Опыт использования:</b> {review.usagePeriod}</p>
                    <p className="reviews-box__paragraph"><span
                        className="review-box__span"><b>Достоинства:</b></span>
                        <span className="review-box__span">{review.advantages}</span>
                    </p>
                    <p className="reviews-box__paragraph"><span
                        className="review-box__span"><b>Недостатки:</b></span>
                        <span className="review-box__span">{review.disadvantages}</span>
                    </p>
                </div>
            </div>
        </div>
        <div className="br"></div>
        </>
    )
}

export default Review;