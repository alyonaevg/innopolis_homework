import React, {useState} from 'react';
import './MemoryConfigs.css';
import MemoryButton from '../MemoryButton/MemoryButton';

function MemoryConfigs() {
  const configs = [
    {
      id: 1,
      text: "128 ГБ",
      value: "128gb",
    },
    {
      id: 2,
      text: "256 ГБ",
      value: "256gb",
    },
    {
      id: 3,
      text: "512 ГБ",
      value: "512gb",
    },
  ]
  const [activeMemoryButton, setActiveMemoryButton] = useState(1);
  

  function handleClick(e) {
    setActiveMemoryButton(e.key);
  }

  return (
    <div className="memory-configuration specs-reviews-box__memory-configuration">
      <h3 className="memory-configuration__title">Конфигурация памяти: {}</h3>
      <div className="memory-configuration__radio-buttons">
        {configs.map((config) => (
          <MemoryButton 
          key={config.id} 
          memoryConfig={config} 
          active={config.id === activeMemoryButton} 
          onClick={handleClick}
          />
        ))}
      </div>
    </div>
  )
}

export default MemoryConfigs;