import React from 'react'
import './ColorButton.css';

function ColorButton(props) {
    const {color, active, ...restProps} = props;
    const className = `color-configuration__item-image ${active ? 'color-configuration__item-image_active' : ''}`
    return (
        <label>
            <input type="radio" name="color" value={color.value} />
            <div {...restProps} className={className}>
                <img src={color.imgLink} height="60px" alt={color.alt} />
            </div>
        </label>
    )
}

export default ColorButton