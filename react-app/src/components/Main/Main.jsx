import React from "react";
import Colors from "../Colors/Colors";
import MemoryConfigs from "../MemoryConfigs/MemoryConfigs";
import Reviews from '../Reviews/Reviews';
import ReviewForm from '../ReviewForm/ReviewForm';
import Sidebar from '../Sidebar/Sidebar';
import "./Main.css";

function Main() {
    return (<main className="main">
        <nav className="nav">
            <div id="top" className="breadcrumbs nav__breadcrumbs">
                <a className="breadcrumbs__link" href="#">Электроника</a><span>&gt;</span><a className="breadcrumbs__link"
                    href="#">Смартфоны и гаджеты</a><span>&gt;</span><a className="breadcrumbs__link" href="#">Мобильные
                        телефоны</a><span>&gt;</span><a className="breadcrumbs__link" href="#">Apple</a>
            </div>
        </nav>
        <h2 className="item-title main__item-title">Смартфон Apple iPhone 13</h2>
        <div className="item-images main__item-images">
            <img src="resources/white_1.jpg" alt="iPhone изображение 1" />
            <img src="resources/white_2.jpg" alt="iPhone изображение 2" />
            <img src="resources/white_3.jpg" alt="iPhone изображение 4" />
            <img src="resources/all_colors.jpg" alt="iPhone изображение 3" />
        </div>
        <div className="specs-reviews-aside-box main__specs-reviews-aside-box">
            <div className="specs-reviews-box">
                <Colors />
                <MemoryConfigs />
                <section className="specs">
                    <h3 className="specs__title">Характеристики товара</h3>
                    <ul className="specs__list">
                        <li>Экран: <b>6.1</b></li>
                        <li>Встроенная память: <b>128 ГБ</b></li>
                        <li>Операционная система: <b>iOS 15</b></li>
                        <li>Беспроводные интерфейсы: <b>NFC, Bluetooth, Wi-Fi</b></li>
                        <li>Процессор: <a className="specs__link" href="https://ru.wikipedia.org/wiki/Apple_A15"
                            target="_blank"><b>Apple
                                A15
                                Bionic</b></a>
                        </li>
                        <li>Вес: <b>173 г</b></li>
                    </ul>
                </section>
                <section className="description">
                    <h3 className="description__title">Описание</h3>
                    <p className="description__paragraph">Наша самая совершенная система двух камер.<br />
                        Особый взгляд на прочность дисплея.<br />
                        Чип, с которым всё супербыстро.<br />
                        Аккумулятор держится заметно дольше.<br />
                        <i>iPhone 13 - сильный мира всего.</i>
                    </p>
                    <p className="description__paragraph">Мы разработали совершенно новую схему расположения и
                        развернули объективы на 45 градусов.
                        Благодаря
                        этому
                        внутри корпуса поместилась наша лучшая система двух камер с увеличенной матрицей
                        широкоугольной
                        камеры.
                        Кроме того, мы освободили место для системы оптической стабилизациии зображения сдвигом
                        матрицы.
                        И
                        повысили
                        скорость работы матрицы на сверхширокоугольной камере.</p>
                    <p className="description__paragraph">Новая сверхширокоугольная камера видит больше деталей в тёмных
                        областях снимков. Новая
                        широкоугольная
                        камера
                        улавливает на 47% больше света для более качественных фотографий и видео. Новая оптическая
                        стабилизация
                        сосдвигом матрицы обеспечит чёткие кадры даже в неустойчивом положении.</p>
                    <p className="description__paragraph">Режим «Киноэффект» автоматически добавляет великолепные эффекты
                        перемещения фокуса и изменения
                        резкости.
                        Просто начните запись видео. Режим «Киноэффект» будет удерживать фокус на объекте съёмки,
                        создавая
                        красивый
                        эффект размытия вокруг него. Режим «Киноэффект» распознаёт, когда нужно перевести фокус на
                        другого человека
                        или объект, который появился в кадре. Теперь ваши видео будут смотреться как настоящее кино.
                    </p>
                </section>
                <section className="comparison">
                    <h3 className="comparison__title">Сравнение моделей</h3>
                    <table className="comparison__table">
                        <thead>
                            <tr className="comparison__table-head">
                                <th className="comparison__table-th">Модель</th>
                                <th className="comparison__table-th">Вес</th>
                                <th className="comparison__table-th">Высота</th>
                                <th className="comparison__table-th">Ширина</th>
                                <th className="comparison__table-th">Толщина</th>
                                <th className="comparison__table-th">Чип</th>
                                <th className="comparison__table-th">Объем памяти</th>
                                <th className="comparison__table-th">Аккумулятор</th>
                            </tr>
                        </thead>
                        <tbody className="comparison__table-boby">
                            <tr className="comparison__table-row">
                                <td className="comparison__table-td">Iphone 11</td>
                                <td className="comparison__table-td">194 гр</td>
                                <td className="comparison__table-td">150.9 мм</td>
                                <td className="comparison__table-td">75.7 мм</td>
                                <td className="comparison__table-td">8.3 мм</td>
                                <td className="comparison__table-td">A13 Bionic chip</td>
                                <td className="comparison__table-td">до 128 ГБ</td>
                                <td className="comparison__table-td">до 17 часов</td>
                            </tr>
                            <tr className="comparison__table-row">
                                <td className="comparison__table-td">Iphone 12</td>
                                <td className="comparison__table-td">164 гр</td>
                                <td className="comparison__table-td">146.7 мм</td>
                                <td className="comparison__table-td">71.5 мм</td>
                                <td className="comparison__table-td">7.4 мм</td>
                                <td className="comparison__table-td">A14 Bionic chip</td>
                                <td className="comparison__table-td">до 256 ГБ</td>
                                <td className="comparison__table-td">до 19 часов</td>
                            </tr>
                            <tr className="comparison__table-row">
                                <td className="comparison__table-td">Iphone 13</td>
                                <td className="comparison__table-td">174 гр</td>
                                <td className="comparison__table-td">146.7 мм</td>
                                <td className="comparison__table-td">71.5 мм</td>
                                <td className="comparison__table-td">7.65 мм</td>
                                <td className="comparison__table-td">A15 Bionic chip</td>
                                <td className="comparison__table-td">до 512 ГБ</td>
                                <td className="comparison__table-td">до 19 часов</td>
                            </tr>
                        </tbody>
                    </table>
                </section>
                <Reviews />
                <ReviewForm />
            </div>
            <aside className="aside-box">
                <Sidebar />
                <div className="ads-box aside-box__ads-box">
                    <p className="ads-box__title">Реклама</p>
                    <iframe className="ad" src="ads.html">Реклама</iframe>
                    <iframe className="ad" src="ads.html">Реклама</iframe>
                </div>
            </aside>
        </div>
    </main>);
}

export default Main;