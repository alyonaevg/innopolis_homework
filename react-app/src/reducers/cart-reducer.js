import { createSlice } from "@reduxjs/toolkit";


function getCartFromStorage() {
    const products = localStorage.getItem('cart');
    if (products === null) {
        return [];
    } else {
        const initialProducts = JSON.parse(products);
        if (Array.isArray(initialProducts)) {
            return initialProducts;
        } else {
            return [];
        }
    
    };

}

//создаем переменную, которая вызывает функцию, котрая в качестве аргумента 
//принимает вот такой большой объект
export const cartSlice = createSlice({
    //название "части" хранилища
    name: "cart",

    // Начальное состояние хранилища, когда мы только открывем страницу, товаров нет
    initialState: {
        products: getCartFromStorage(),
        products: getCartFromStorage(),
    },

    // Все доступные метода
    reducers: {
        // Добавить товар, первый параметр это текущее состояние
        // А второй параметр имеет данные для действия
        addProduct: (prevState, action) => {
            // Внутри action.payload информация о добавленном товаре
            // Возвращаем новый массив товаров вместе с добавленным
            const cart = [...prevState.products, action.payload];
            localStorage.setItem('cart', JSON.stringify(cart));
            return {
                ...prevState, 
                products: cart,
            };
        },

        removeProduct: (prevState, action) => {
            const cart = prevState.products.filter((product) => product.id !== action.payload.id);
            localStorage.setItem('cart', JSON.stringify(cart));
            return {
                ...prevState,
                products: cart,
            }
        }
    },
});

// Экспортируем наружу все действия
export const { addProduct, removeProduct } = cartSlice.actions;
// И сам редуктор тоже
export default cartSlice.reducer;