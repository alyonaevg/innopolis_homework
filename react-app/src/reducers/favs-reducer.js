import { createSlice } from "@reduxjs/toolkit";

export const favsSlice = createSlice({
    //название "части" хранилища
    name: "favs",

    // Начальное состояние хранилища, когда мы только открывем страницу, товаров нет
    initialState: {
        products: [],
    },

    // Все доступные метода
    reducers: {
        // Добавить товар, первый параметр это текущее состояние
        // А второй параметр имеет данные для действия
        addFavs: (prevState, action) => {
            return {
                ...prevState,
                // Внутри action.payload информация о добавленном товаре
                // Возвращаем новый массив товаров вместе с добавленным
                products: [...prevState.products, action.payload],
            };
        },

        removeFavs: (prevState, action) => {
            return {
                ...prevState,
                products: prevState.products.filter((product) => product.id !== action.payload.id)
            }
        }
    },
});

// Экспортируем наружу все действия
export const { addFavs, removeFavs } = favsSlice.actions;
// И сам редуктор тоже
export default favsSlice.reducer;