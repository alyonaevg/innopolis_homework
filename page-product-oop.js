'use strict';
//////ДЗ 30, БЕЗ ДОПОЛНИТЕЛЬНОГО ЗАДАНИЯ
class ReviewForm {
    constructor(form, storage) {
        this.form = form;
        this.nameInput = form.querySelector('.review-form__input-text');
        this.ratingInput = form.querySelector('.review-form__input-rating');
        this.textareaInput = form.querySelector('.review-form__textarea');
        this.warningName = form.querySelector('.review-form__warning-name');
        this.warningRating = form.querySelector('.review-form__warning-rating');
        this.storage = storage;
        this.savedInputs = [this.nameInput, this.ratingInput, this.textareaInput];
    }

    handleSubmit(event) {
        event.preventDefault();
        if (this.nameInputCorrect()) {
            if (this.ratingInputCorrect()) {
                this.clearSavedInputs();
            }
        }
    }

    nameInputCorrect() {
        if (!this.nameInput.value) {
            this.showWarningMessage(this.warningName, 'Вы забыли указать имя и фамилию');
            this.changeBorderColor(this.nameInput);
            return false;
        }

        if (this.nameInput.value.length < 2) {
            this.showWarningMessage(this.warningName, 'Имя не может быть короче 2-х символов');
            this.changeBorderColor(this.nameInput);
            return false;
        }
        return true;
    }

    ratingInputCorrect() {
        if (!this.ratingInput.value || typeof (+this.ratingInput.value) !== 'number' || this.ratingInput.value < 1 || this.ratingInput.value > 5) {
            this.showWarningMessage(this.warningRating, 'Оценка должна быть от 1 до 5');
            this.changeBorderColor(this.ratingInput);

            return false;
        }
        return true;
    }

    changeBorderColor(input) {
        input.style.border = '1px solid #DA4A0C';
    }

    showWarningMessage(warning, message) {
        warning.innerHTML = message;
        warning.style.display = "inline-block";
    }

    handleFocus(event) {
        event.target.style.border = '';
        let warningMessages = document.querySelectorAll('span.review-form__validation');
        warningMessages.forEach((message) => message.style.display = 'none');
    }

    handleLoad(event) {
        let savedInputValuesJson = this.storage.getItem('formValues');
        if (!savedInputValuesJson) return;
        let savedInputValues = JSON.parse(savedInputValuesJson);                                   
        this.savedInputs.forEach((savedInput) => {
            savedInput.value = savedInputValues[savedInput.name];
        })
    }

    handleBeforeunload(event) {
        event.preventDefault()
        let savedInputValues = {};
        this.savedInputs.forEach((savedInput) => {
            savedInputValues[savedInput.name] = savedInput.value;
        });
        this.storage.setItem('formValues', JSON.stringify(savedInputValues));
    }


    clearSavedInputs() {
        this.savedInputs.forEach((savedInput) => {
            this.storage.removeItem('formValues');
            savedInput.value = '';
        })
    }
}

let myReviewForm = new ReviewForm(document.querySelector('.review-form'), window.localStorage);

myReviewForm.form.addEventListener('submit', myReviewForm.handleSubmit.bind(myReviewForm));
myReviewForm.nameInput.addEventListener('focus', myReviewForm.handleFocus.bind(myReviewForm));
myReviewForm.ratingInput.addEventListener('focus', myReviewForm.handleFocus.bind(myReviewForm));
window.addEventListener('load', myReviewForm.handleLoad.bind(myReviewForm));
window.addEventListener('beforeunload', myReviewForm.handleBeforeunload.bind(myReviewForm));


