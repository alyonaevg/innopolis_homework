"use strict";

//Упражнение 1

/**
 * Проверяет отсутсвие ключей в объекте
 * @param {any} someObject Объект для проверки
 * @returns {boolean} true если ключей нет, false если ключи есть
 */
function isEmpty(someObject) {

    for (let key in someObject) {
        return false
    }

    return true;
}

let user1 = { age: 12 };
console.log(isEmpty(user1));

let user2 = {};
console.log(isEmpty(user2));

//Упражнение 2 см. файл data.js

//Упражнение 3

let salaries = {
    John: 100000,
    Ann: 160000,
    Pete: 130000,
};

/**
 * Увеличивает числовые значения (зп сотрудников) в объекте на n-ое количество процентов
 * @param {number} percent процент на которое увеличатся числовые значение
 * @returns {object} новый объект с увеличинными числами
 */
function raiseSalary(percent) {
    let newSalaries = {};

    for (let key in salaries) {
        if (typeof salaries[key] === "number") {
            let newSalary = salaries[key] / 100 * percent + salaries[key];
            let newSalaryRounded = Math.floor(newSalary);
            newSalaries[key] = newSalaryRounded;
        }
    }
    return newSalaries;
}

function totalBudget(salariesToCount) {
    if (typeof salariesToCount !== 'object') {
        return undefined;
    }

    let totalAmount = 0;
    for (const key in salariesToCount) {
        totalAmount = totalAmount + salariesToCount[key];
    }
    return totalAmount;
}

let raisedSalaries = raiseSalary(5);
console.log(raisedSalaries);

console.log(totalBudget(raisedSalaries))