'use strict';

// Упражнение 1

function getSumm(arr) {
    let result = 0;
    arr.forEach((n) => {
        if (typeof n === 'number') return result = result + n;
    })
    return result;
}

let arr1 = [1, 2, 10, 5];
alert(getSumm(arr1));// 18

let arr2 = ["a", {}, 3, 3, -2];
alert(getSumm(arr2));// 4

// Упражнение 2 --> data.js

//Упражнение 3

let cart= [4884];

function addToCart(item) { 
    if (cart.includes(item) === false) cart.push(item);
}

function removeFromCart(item) {
    cart.pop(item);
}

// Добавили товар
addToCart(3456);
console.log(cart);// [4884, 3456]

// Повторно добавили товар
addToCart(3456);
console.log(cart);// [4884, 3456]

// Удалили товар
removeFromCart(4884);
console.log(cart);// [3456]